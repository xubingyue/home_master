/**
*
* @brief	LED指示灯
* @author	yun
* @data		2016-12-27
* @desc		LED指示灯驱动
* @version	v0.1
*
*/

#include	"platform_led.h"


int platform_led_init(void){
	return 0;
}

// 开灯
int platform_led_on(platform_led_t led){
	return 0;
}

// 关灯
int platform_led_off(platform_led_t led){
	return 0;
}

