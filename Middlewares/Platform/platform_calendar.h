/**
*
* @brief	Calendar
* @author	yun
* @date		2016-12-21
* @desc
* @version
*
*/


#ifndef		__PLATFORM_CALENDAR_H
#define		__PLATFORM_CALENDAR_H

int platform_calendar_init(void);

int platform_calendar_set(char *time);

int platform_calendar_show(void);

int platform_calendar_get(char * buffer);

int platform_calendar_test(void);

#endif
